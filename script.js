import {utils} from "./utils.js";

function init()
{
    let ignoreNextLocalStoreChanging = false;

    function Popup(args)
    {
        const that = this;
        const $root = $('<div class="bavi-ebay-popup">');
        let inBody = false;
        const $textarea = $('<textarea>');
        const $title = $('<input type="text">');
        const $action = $('<div class="bavi-ebay-popup__action">');
        const $save = $('<div class="bavi-ebay-popup__btn">Save</div>');
        const $close = $('<div class="bavi-ebay-popup__btn">Close</div>');
        let targetSaveCb = null;

        $close.appendTo($action);
        $save.appendTo($action);
        $title.appendTo($root);
        $textarea.appendTo($root);
        $action.appendTo($root);

        that.setData = function (data)
        {
            $title.val(data.title);
            $textarea.val(data.text);
        }

        that.toggleVisibility = function (visible, saveCb)
        {
            if (!inBody)
            {
                $root.appendTo($('body'));
                inBody = true;
            }
            $root.toggleClass('bavi-ebay-popup--visible', visible);
            if (visible) targetSaveCb = saveCb;
        }
        $save.click(function ()
        {
            targetSaveCb({
                title: $title.val(),
                text: $textarea.val(),
            });
            that.toggleVisibility(false);
        });
        $close.click(function ()
        {
            that.toggleVisibility(false);
        });
    }

    function Note(args)
    {
        const that = this;
        const $parent = args.$parent;
        const url = args.url;
        if (!url) return;

        const defaults = {
            url: url,
            notes: {
                title: '',
                text: ''
            }
        };
        let data;
        const $customBox = $('<div class="bavi-ebay-note__custom-box"></div>');
        const $noteBtn = $('<div class="bavi-ebay-note__show-btn"></div>');
        const $title = $('<div class="bavi-ebay-note__title"></div>');

        that.loadDataFromStorage = function ()
        {
            data = storage[url] !== undefined ? storage[url] : defaults;
        }

        that.updateStatus = function ()
        {
            $parent.toggleClass('bavi-ebay-note--exist', !!data.notes.text.length || !!data.notes.title.length);
            $title.text(data.notes.title);
        }

        function onSave(notes)
        {
            data.notes = notes;
            storage[url] = data;
            updateStorage(storage);
            that.updateStatus();
        }

        $noteBtn.on('click', function (e)
        {
            popup.setData(data.notes);
            popup.toggleVisibility(true, onSave);
        });

        $parent.addClass('bavi-ebay-note')
        $parent.css('position', 'relative');
        $noteBtn.appendTo($customBox);
        $title.appendTo($customBox);
        $customBox.appendTo($parent);

        that.loadDataFromStorage();
        that.updateStatus();
    }

    function getStorage()
    {
        const str = localStorage.getItem('baviLn');
        if (!str) return {};
        return JSON.parse(str);
    }

    function updateStorage(data)
    {
        localStorage.setItem('baviLn', JSON.stringify(data));
        window.dispatchEvent(new Event("storage"));
        ignoreNextLocalStoreChanging = true;
    }

    const popup = new Popup();
    let storage = getStorage();

    /**
     * @type {Array.<Note>}
     */
    const notes = [];

    $('.ad-listitem').each(function ()
    {
        const $parent = $(this);
        const url = $parent.find('.aditem').data('href');
        if (!url) return true;

        const note = new Note({
            $parent: $(this),
            url: url,
        });
        notes.push(note);
    });

    const $contactBox = $('#viewad-contact-box');
    if ($contactBox.length)
    {
        const note = new Note({
            $parent: $contactBox,
            url: window.location.pathname,
        });
        notes.push(note);
    }

    addEventListener('storage', (event) =>
    {
        if (ignoreNextLocalStoreChanging)
        {
            ignoreNextLocalStoreChanging = false;
            return;
        }
        storage = getStorage();
        notes.forEach(function (note)
        {
            note.loadDataFromStorage();
            note.updateStatus();
        });
    });
}

utils.ready(init);
