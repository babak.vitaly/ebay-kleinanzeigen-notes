export const utils = {};

utils.ready = function (fn)
{
    if (document.readyState !== 'loading')
    {
        fn();
        return;
    }
    document.addEventListener('DOMContentLoaded', fn);
}